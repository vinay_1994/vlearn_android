import React, {Component} from 'react';
import {StyleSheet, Image} from 'react-native';
import {Container, Button, Icon, Text} from 'native-base';
import Picker from 'react-native-image-picker';
import axios from '../../../utils/interceptor';

export default class uploader extends Component {
  state = {
    fileDataUri: null,
  };

  openGallary = () => {
    Picker.launchImageLibrary(
      {
        title: 'Select Image',
        customButtons: [
          {
            name: 'customOptionKey',
            title: 'Choose file from Custom Option',
          },
        ],
        storageOptions: {
          skipBackup: true,
          path: 'images',
        },
      },
      (res) => {
        if (res.didCancel) {
          console.log('User cancelled image picker');
        } else if (res.error) {
          console.log('ImagePicker Error: ', res.error);
        } else if (res.customButton) {
          console.log('User tapped custom button: ', res.customButton);
          alert(res.customButton);
        } else {
          this.setState(
            {
              fileDataUri: res.uri,
            },
            () => {
              try {
                let formData = new FormData();
                formData.append('file', {
                  uri: this.state.fileDataUri,
                  type: 'image/jpeg',
                  name: 'file.jpeg',
                });
                console.log(formData);
                axios
                  .post('/dashboard/upload', formData)
                  .then((res) => {
                    console.log(res.data);
                  })
                  .catch((rej) => console.log('rejected', rej));
              } catch (error) {
                console.log('from err', error);
              }
            },
          );
        }
      },
    );
  };

  render() {
    return (
      <Container style={styles.container}>
        <Button onPress={() => this.openGallary()} iconLeft>
          <Icon name="home" type="Ionicons" />
          <Text>Upload</Text>
        </Button>
        <Button onPress={() => this.props.closeUploaderModal()} iconLeft>
          <Icon name="close-outline" type="Ionicons" />
          <Text>Close</Text>
        </Button>
        <Image
          source={{uri: this.state.fileDataUri}}
          style={{width: 200, height: 200}}
        />
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
