import React, {Component} from 'react';
import {
  Container,
  H1,
  Content,
  List,
  ListItem,
  Thumbnail,
  Text,
  Left,
  Body,
  Right,
  Button,
  Spinner,
} from 'native-base';

export default class VideoPage extends Component {
  render() {
    console.log(this.props.data);
    return !this.props.data ? (
      <Spinner color="green" />
    ) : (
      <Container>
        <Content>
          {this.props.data.VIDEO.length === 0 ? (
            <H1
              style={{
                padding: 150,
              }}>
              Empty
            </H1>
          ) : (
            this.props.data.VIDEO.map((i, key) => (
              <List key={key}>
                <ListItem thumbnail>
                  <Left>
                    <Thumbnail
                      square
                      source={require('../../../static/images/video.png')}
                    />
                  </Left>
                  <Body>
                    <Text>{i.original_filename}</Text>
                    <Text note numberOfLines={1}>
                      Size : {Number(i.file_size) / 100} Kb
                    </Text>
                  </Body>
                  <Right>
                    <Button transparent>
                      <Text>Play</Text>
                    </Button>
                  </Right>
                </ListItem>
              </List>
            ))
          )}
        </Content>
      </Container>
    );
  }
}
