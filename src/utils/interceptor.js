import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import Constants from './../shared/constants';

axios.interceptors.request.use(
  async function (config) {
    try {
      let token = await AsyncStorage.getItem('login_token');
      config.baseURL = Constants.baseUrl;
      config.headers = {
        Authorization: `Bearer ${token}`,
      };
      return config;
    } catch (error) {
      console.log(error);
    }
  },
  function (error) {
    // Do something with request error
    return Promise.reject(error);
  },
);

export default axios;
