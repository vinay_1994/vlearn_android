import React from 'react';
import {View, Text} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import DashboardRotes from './src/routing';
import LandingPage from './src/screens/home';

const StackNavigator = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <StackNavigator.Navigator initialRouteName="Home">
        <StackNavigator.Screen name="Home" component={LandingPage} />
        <StackNavigator.Screen name="Dashboard" component={DashboardRotes} />
        <StackNavigator.Screen name="Register" component={Register} />
      </StackNavigator.Navigator>
    </NavigationContainer>
  );
}

const Register = () => {
  return <Text>from regidter</Text>;
};
